#/usr/bin/python
# -*- coding: utf-8 -*-

# Script takes infile: data or Z+jets MC and produces outfile with
# - kinematic variables relevant to MET trigger SF derivation including new met definition
# - MET' binned histogram of all Z candidate events/those passing trigger
# MET' = nominal MET + dimuon pT

# CAREFUL!! Data18 + Zjets input files contain all analysis triggers whilst data15/16/17 contain
#           only those necessary for these studies. Careful when requiring event to pass trigger

from ROOT import TH1D, TCanvas, TTree, TFile, TLegend, gPad, gStyle, TLatex, TLorentzVector, TBranch, TVector2
from array import array
import numpy as np
import os

def main(infile, wdir, year):

  #fileID = infile.split(".")[1] + "_" + infile.split("_")[-2]
  fileID = infile.split(".root")[0]
  print("fileID: ", fileID)
  outdir = 'output/hTrig/'+year+'/'
  # input and output trees
  tfile = TFile.Open(wdir+infile, "READ")
  outFile = TFile.Open(outdir+fileID+"_hTrig_new.root", "RECREATE")
  ttree = tfile.Get("miniT") 
  outtree = TTree("zEvents_tree", "zEvents_tree")
  h_noTrig = TH1D("h_noTrig", "MET Distribution before MET trigger", 25, 50e3, 300e3)
  h_noTrig.Sumw2()
  h_noTrig.SetDirectory(0)
  #h_metTrig = TH1D("h_trigEff", "Z+jets vs. Run 2 Data: MET Trigger Efficiency vs. MET + Dimuon pT Distribution", 50, 50e3, 280e3) # clone later to make trigEff hist
  h_metTrig = TH1D("h_metTrig", "MET Distribution after MET trigger", 25, 50e3, 300e3) # clone later to make trigEff hist
  h_metTrig.Sumw2()
  h_metTrig.SetDirectory(0)
  
  # arrays to store variables
  m_weight = array('d', [0])
  m_njets = array('i', [0])
  m_njets30 = array('i', [0])
  m_metTrig = array('b', [0])
  m_muon_pT = array('d', [0, 0])
  m_muon1_pT = array('d', [0, 0])
  m_muon2_pT = array('d', [0, 0])
  m_muon_eta = array('d', [0, 0])
  m_muon_phi = array('d', [0, 0])
  m_muon_e = array('d', [0, 0])
  m_dimuon_mass = array('d', [0])
  m_dimuon_pT = array('d', [0])
  m_Z_pT = array('d', [0])
  m_dimuon_dR = array('d', [0])
  m_met = array('d', [0])
  m_dimuon_met = array('d', [0])

  # add Branches  
  outtree.Branch("weight", m_weight, 'weight/D')
  outtree.Branch("njets", m_njets, 'njets/I')
  outtree.Branch("njets30", m_njets30, 'njets30/I')
  outtree.Branch("metTrig", m_metTrig, 'm_metTrig/B')
  outtree.Branch("muon_pT", m_muon_pT, 'muon_pT/D')
  outtree.Branch("muon1_pT", m_muon1_pT, 'muon1_pT/D')
  outtree.Branch("muon2_pT", m_muon2_pT, 'muon2_pT/D')
  outtree.Branch("muon_eta", m_muon_eta, 'muon_eta/D')
  outtree.Branch("muon_phi", m_muon_phi, 'muon_phi/D')
  outtree.Branch("muon_e", m_muon_e, 'muon_e/D')
  outtree.Branch("dimuon_mass", m_dimuon_mass, 'dimuon_mass/D')
  outtree.Branch("dimuon_pT", m_dimuon_pT, 'dimuon_pT/D')
  outtree.Branch("Z_pT", m_Z_pT, 'Z_pT/D')
  outtree.Branch("dimuon_dR", m_dimuon_dR, 'dimuon_dR/D')
  outtree.Branch("met", m_met, 'met/D')
  outtree.Branch("dimuon_met", m_dimuon_met, 'dimuon_met/D')

  noTrig_count, metTrig_count = 0, 0
  count_a, count_15, count_16 = 0, 0, 0
  sumOfWeights = tfile.numEvents.GetBinContent(2)
  for entry in range(ttree.GetEntries()):
    # zjet MC files contains 2015 and 2016 together in same pu-campaign
    # separate years 2015 and 2016 for year specific trigger efficiencies 
    if entry > 0 and entry%100000==0: print("Processed {} of {} entries".format(entry,ttree.GetEntries()))
    ttree.GetEntry(entry)

    evt_is15 = ttree.RunNumber in range(266904, 284485)
    evt_is16 = ttree.RunNumber in range(296939, 311482)
    evt_is16a = ttree.RunNumber in range(296939, 302872)
    evt_is16b = ttree.RunNumber in range(302872, 311482)
    evt_is17 = ttree.RunNumber in range(324320, 341650)
    evt_is18 = ttree.RunNumber in range(348197, 364486)
    evt_isRun2 = evt_is15 or evt_is16 or evt_is17 or evt_is18
    if evt_isRun2 == False: continue
    #print("evt_is15, evt_is16, evt_is17, evt_is18: ", evt_is15, evt_is16, evt_is17, evt_is18)
   
    if "Sherpa" in infile and "r9364" in infile:
      # want 2015 events but event is from 2016
      if year == "2015" and evt_is15 == False: continue
      # want 2016 events but event is from 2015
      if year == "2016" and evt_is16 == False: continue

    # calculate weight
    if ttree.isMC: 
      #if ttree.RunNumber == 0: continue
      if evt_is15 or evt_is16: intLumi = 36.1
      if evt_is17: intLumi = 44.3
      if evt_is18: intLumi = 58.45
      weight = intLumi * ttree.amiXsection * 1000. * ttree.weight * ttree.filterEff * ttree.kFactor / sumOfWeights
    if not ttree.isMC: weight = ttree.weight

    # jets selection    
    njets30 = 0
    njets = ttree.jet_cal_pt.size() 
    for idx_jet in range(njets):
      if ttree.jet_cal_pt.at(idx_jet) > 30e3 and ttree.jet_cal_isSTDOR.at(idx_jet): njets30+=1
    #if njets30 > 2: continue

    # Single muon trigger 
    if "Sherpa" in inFile or "data18" in inFile:
      if ttree.RunNumber in range(296938, 364485) and ttree.trig.at(11) == 0: continue  # 2016/17/18 single muon trigger   DATA18/ZJETS SAMPLES
      if ttree.RunNumber in range(267069, 284668) and ttree.trig.at(12) == 0: continue  # 2015 single muon trigger         DATA18/ZJETS SAMPLES
    else:
      if ttree.RunNumber in range(296938, 364486) and ttree.trig.at(0) == 0: continue  # 2016/17/18 single muon trigger     DATA15/16/17 SAMPLES
      if ttree.RunNumber in range(266904, 284485) and ttree.trig.at(1) == 0: continue  # 2015 single muon trigger           DATA15/16/17 SAMPLES

    # Standard ATLAS Z selection 
    Z_vars = isZ_event(ttree, entry)
    if Z_vars == [-99, -1, -1]: continue
    #Z_count += 1
    Zmu1 = Z_vars[1]
    Zmu2 = Z_vars[2]
    mu1_p4 = TLorentzVector(0, 0, 0, 0) 
    mu2_p4 = TLorentzVector(0, 0, 0, 0) 
    mu1_p4.SetPtEtaPhiE(ttree.muon_pt.at(Zmu1), ttree.muon_eta.at(Zmu1), ttree.muon_phi.at(Zmu1), ttree.muon_e.at(Zmu1)) 
    mu2_p4.SetPtEtaPhiE(ttree.muon_pt.at(Zmu2), ttree.muon_eta.at(Zmu2), ttree.muon_phi.at(Zmu2), ttree.muon_e.at(Zmu2)) 
    dimuon_mass = (mu1_p4 + mu2_p4).M()
    dimuon_pT = (mu1_p4 + mu2_p4).Pt()
    dimuon_dR = mu1_p4.DeltaR(mu2_p4)
    dimuon_met = Z_vars[0]
 
    m_weight[0] = weight
    # fill met Branches
    m_njets[0] = njets
    m_njets30[0] = njets30
    m_metTrig[0] = 0 # set o zero by default
    m_met[0] = ttree.MET
    m_dimuon_met[0] = dimuon_met
    # single muon Branches
    m_muon_pT[0] = ttree.muon_pt.at(Zmu1)
    m_muon_eta[0] = ttree.muon_eta.at(Zmu1)
    m_muon_phi[0] = ttree.muon_phi.at(Zmu1)
    m_muon_e[0] = ttree.muon_e.at(Zmu1)
    m_muon_pT[1] = ttree.muon_pt.at(Zmu2)
    m_muon_eta[1] = ttree.muon_eta.at(Zmu2)
    m_muon_phi[1] = ttree.muon_phi.at(Zmu2)
    m_muon_e[1] = ttree.muon_e.at(Zmu2)
    # dimuon quantities
    m_dimuon_mass[0] = dimuon_mass
    m_dimuon_pT[0] = dimuon_pT
    m_dimuon_dR[0] = dimuon_dR
    m_muon1_pT[0] = ttree.muon_pt.at(Zmu1)
    m_muon2_pT[0] = ttree.muon_pt.at(Zmu2)
    m_Z_pT[0] = ttree.ptZ

    #if "data18" in inFile or "Sherpa" in inFile: metTrig15_idx, metTrig16a_idx, metTrig16b_onwards_range = 28, 27, range(24, 27, 1)
    #else: metTrig15_idx, metTrig16a_idx, metTrig16b_onwards_range = 6, 5, range(2, 5, 1)

    skim_trigs = [6, 5, 4, 3, 2]
    all_trigs = [28, 27, 26, 25, 24]
    runNums = [evt_is15, evt_is16a, evt_is16b, evt_is17, evt_is18]
    
    h_noTrig.Fill(dimuon_met, weight)
    # loop through periods and relevant triggers
    # check if trigger is passed, fill histogram if so
    for idx in range(len(runNums)):
      if "data18" in inFile or "Sherpa" in inFile: 
        if runNums[idx] and ttree.trig.at(all_trigs[idx]):
          h_metTrig.Fill(dimuon_met, weight)
          metTrig_count+=1
          metTrig = 1
      else:
        if runNums[idx] and ttree.trig.at(skim_trigs[idx]):
          h_metTrig.Fill(dimuon_met, weight)
          metTrig_count+=1
          metTrig = 1

    '''# Fill histograms
    h_noTrig.Fill(dimuon_met, weight)
    noTrig_count+=1
    # fill MET triggered histogram
    if evt_is15 and ttree.trig.at(metTrig15_idx):
      h_metTrig.Fill(dimuon_met, weight)
      metTrig_count+=1
    if evt_is16a and ttree.trig.at(metTrig16a_idx):
      h_metTrig.Fill(dimuon_met, weight)
      metTrig_count+=1
    if evt_is16b and ttree.trig.at(metTrig16b_idx) 
      h_metTrig.Fill(dimuon_met, weight)
      metTrig_count+=1 
    if evt_is17 and ttree.trig.at(metTrig17_idx) 
      h_metTrig.Fill(dimuon_met, weight)
      metTrig_count+=1 
    if evt_is18 and ttree.trig.at(metTrig18_idx):
      h_metTrig.Fill(dimuon_met, weight)
      metTrig_count+=1''' 

    outtree.Fill() 
  print("Loop completed, miniT.GetEntries():", ttree.GetEntries())
  outtree.Write()
  #print("Number of Z events total vs. those passing MET criteria: ", Z_count, count)
  print("noTrig count vs metTrig count: ", noTrig_count, metTrig_count)
  #if "Sherpa" in inFile: print("Number of events in zjets_a vs. those from 2015, 2016: ", count_a, count_15, count_16)
  # write histograms to output file
  h_trigEff = h_metTrig.Clone()
  h_trigEff.SetNameTitle("h_trigEff", "MET Trigger Efficiency vs. Proxy Offline MET")
  h_trigEff.Divide(h_trigEff, h_noTrig, 1,  1, "B")  
  h_trigEff.Draw()
  h_noTrig.Draw()
  h_metTrig.Draw()
  h_noTrig.Write()
  h_metTrig.Write()
  h_trigEff.Write()
  #outFile.Write()
  outFile.Close()
  tfile.Close() 

def isZ_event(ttree, entry):
  
  mu_idxs, good_muons = [], []
  muons_found = False
  Zmu1_idx = -1
  Zmu2_idx = -1
  isZ = False
  dimuon_met = -99

  # find exactly two oppositely charged, combined, medium isolated muons
  for mu_idx in range(len(ttree.muon_pt)):
    mu_iso = ttree.muon_author.at(mu_idx) == 1                                        # require isolated muons
    mu_comb = ttree.muon_type.at(mu_idx) == 0                                         # require combined muons
    if mu_iso and mu_comb: mu_idxs.append(mu_idx)
  
  if len(mu_idxs) == 2 and (ttree.muon_charge.at(0) + ttree.muon_charge.at(1)) == 0:     # oppositely charged muons
    for mu2_idx in range(len(ttree.muon_pt)):
      mu_pt = ttree.muon_pt.at(mu2_idx) > 20e3                                           # minimum muon pT 
      mu_eta = abs(ttree.muon_eta.at(mu2_idx)) < 2.4                                     # muon eta cut
      mu_signal = ttree.muon_isSignal[mu2_idx] == 1                                      # require signal muon quality cuts
      if mu_pt and mu_eta and mu_signal: good_muons.append(mu2_idx)

  if len(mu_idxs) > 2:
    for mu1_idx in mu_idxs:
      mu1_charge = ttree.muon_charge.at(mu1_idx)
      for mu2_idx in mu_idxs:
        if (mu2_idx == mu1_idx): continue
        mu2_charge = ttree.muon_charge.at(mu2_idx)
        # want EXACTLY 2 muons with opposite charge
        if (mu1_charge + mu2_charge) == 0 and muons_found == True: break
        if (mu1_charge + mu2_charge) == 0 and muons_found == False: 
          muons_found = True
          good_muons.append(mu1_idx)
          good_muons.append(mu2_idx)
          break
  
  if len(good_muons) == 2: 
    mu1_p4 = TLorentzVector(0, 0, 0, 0)
    mu2_p4 = TLorentzVector(0, 0, 0, 0)
    mu1_p4.SetPtEtaPhiE(ttree.muon_pt.at(good_muons[0]), ttree.muon_eta.at(good_muons[0]), ttree.muon_phi.at(good_muons[0]), ttree.muon_e.at(good_muons[0]))
    mu2_p4.SetPtEtaPhiE(ttree.muon_pt.at(good_muons[1]), ttree.muon_eta.at(good_muons[1]), ttree.muon_phi.at(good_muons[1]), ttree.muon_e.at(good_muons[1]))
    dimuon_mass = (mu1_p4 + mu2_p4).M()
    if dimuon_mass > 81e3 and dimuon_mass < 101e3:
      #dimuon_pT = (mu1_p4 + mu2_p4).Pt()
      #dimuon_dR = mu1_p4.DeltaR(mu2_p4)
      Zmu1_idx = good_muons[0]
      Zmu2_idx = good_muons[1]
      isZ = True

    if isZ:
      met_2vec = TVector2(0, 0)
      Zmu1_2vec = TVector2(0, 0)
      Zmu2_2vec = TVector2(0, 0)
      met_2vec.SetMagPhi(ttree.MET, ttree.MET_phi)
      Zmu1_2vec.SetMagPhi(ttree.muon_pt.at(Zmu1_idx), ttree.muon_phi.at(Zmu1_idx))
      Zmu2_2vec.SetMagPhi(ttree.muon_pt.at(Zmu2_idx), ttree.muon_phi.at(Zmu2_idx))
      dimuon_met_p4 = met_2vec + Zmu1_2vec + Zmu2_2vec
      dimuon_met = np.sqrt((dimuon_met_p4.Px()**2) + (dimuon_met_p4.Py()**2))

  return [dimuon_met, Zmu1_idx, Zmu2_idx] 

wdir = '/Volumes/Extreme SSD/zjets_d/'
for inFile in os.listdir(wdir):
  if ".root" in inFile: 
    print("Processing ", inFile)
    main(inFile, wdir, year="2017")

