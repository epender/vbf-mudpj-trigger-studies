from ROOT import TFile, TLegend, TCanvas, gStyle, TH1D, TLine, kGray, TPaveText, TLatex, TRatioPlot, TF1, TMath, TEfficiency, TLatex 
from array import array
import os, argparse
import math
import atlasplots as aplt

def main(year):
  
  wdir = "output/hTrig/"+year+"/"   
  hs_trigEff = []
  fileStr = year.split("0")[1]+'_hTrig.root' 
  #fileNames = ["data"+fileStr, "zjets"+fileStr]
  samples = ["Data", "Zjets"]
  #samples = ["zjets"]
  for idx in range(len(samples)): 
    fName = samples[idx].lower()+fileStr
    tfile = TFile.Open(wdir+fName, "READ") 
    h_noTrig = tfile.Get("h_noTrig")
    h_noTrig.SetDirectory(0)
    h_metTrig = tfile.Get("h_metTrig")
    h_metTrig.SetDirectory(0)
    h_trigEff = h_metTrig.Clone()
    h_trigEff.Divide(h_trigEff, h_noTrig, 1, 1, "B")
    #h_trigEff.SetNameTitle("h_trigEff_"+samples[idx].lower(), "Z+jets vs. "+year+" Data: MET Trigger Efficiency vs. MET + Dimuon pT Distribution")
    h_trigEff.SetNameTitle("h_trigEff_"+samples[idx].lower(), "")
    h_trigEff.SetDirectory(0) 
    hs_trigEff.append(h_trigEff)
    tfile.Close()
  #print(fileNames)
  
  fileColor = [209, 51]
  legend = TLegend(0.13,0.7,0.22,0.8)
  canvas = TCanvas("c", "c", 10, 10, 800, 800)
  canvas.cd()
  gStyle.SetOptStat(0)
  gStyle.SetOptTitle(1)
  gStyle.SetTitleX(0.5) 
  gStyle.SetTitleY(0.98)
  gStyle.SetTitleW(1)
  gStyle.SetTitleH(0.05)
  gStyle.SetTitleSize(0.1, "t")
  
  ratioPlot = TRatioPlot(hs_trigEff[0], hs_trigEff[1])
  # fit error function to ratio plot
  ratioPlot.Draw("HIST E")
  ratioPlot.GetUpperRefYaxis().SetTitle("\mathrm{Lowest\;Unprescaled\;E^{T}_{miss}\;Trigger\;Efficiency}")
  ratioPlot.GetLowerRefYaxis().SetTitle("Data/MC")
  ratioPlot.GetLowerRefYaxis().SetLabelSize(0.03)
  ratioPlot.GetLowerRefXaxis().SetLabelSize(0.03)
  ratioPlot.GetUpperRefYaxis().SetTitleSize(0.03)
  ratioPlot.GetLowerRefYaxis().SetTitleSize(0.03)
  ratioPlot.GetLowerRefYaxis().SetRangeUser(0, 1.1)
  canvas.Update()
 
  for h_idx in range(len(hs_trigEff)):
    hs_trigEff[h_idx].Smooth()
    hs_trigEff[h_idx].SetLineColor(fileColor[h_idx])
    hs_trigEff[h_idx].SetLineWidth(2)
    hs_trigEff[h_idx].Draw("HIST SAME E")
    #hs_trigEff[h_idx].Draw("SAME HIST E")
    hs_trigEff[h_idx].GetXaxis().SetTitle("\mathrm{E^{T}_{miss} + pT_{\mu\mu}\;(GeV)}")
    hs_trigEff[h_idx].GetYaxis().SetTitle("\mathrm{Lowest\;Unprescaled\;E^{T}_{miss}\;Trigger\;Efficiency}")
    hs_trigEff[h_idx].GetYaxis().SetRangeUser(0, 1.1)
    legend.AddEntry(hs_trigEff[h_idx], samples[h_idx]+year.split("0")[1])#fileNames[h_idx].split("_")[0])
  legend.SetTextSize(0.03)
  canvas.Update()
  
  ratioPlot.GetUpperPad().cd()
  # selection text
  #title = TLatex(0.5,0.91,year+" Data-MC Comparison")
  title = TPaveText(150e3,1.13,305e3,1.2)
  vbf_cuts = TPaveText(200e3,0.7,295e3,0.76)
  njets = TPaveText(150e3,0.64,295e3,0.7)
  detajj = TPaveText(150e3,0.58,295e3,0.64)
  mjj = TPaveText(150e3,0.52,295e3,0.58)
  mu_sel = TPaveText(200e3,0.46,295e3,0.52)
  mu_pt = TPaveText(200e3,0.40,295e3,0.46)
  mu_eta = TPaveText(200e3,0.34,295e3,0.4)
  mu_d0 = TPaveText(200e3,0.28,295e3,0.34)
  mu_z0 = TPaveText(200e3,0.22,295e3,0.28)
  mumu_m = TPaveText(200e3,0.16,295e3,0.22)
  mumu_q = TPaveText(200e3,0.1,295e3,0.16)
  mu1_trig = TPaveText(55e3,1.03,100e3,1.07)
 
  '''mu_sel = TPaveText(55e3,0.76,270e3,0.82)
  mu_pt = TPaveText(55e3,0.70,100e3,0.76)
  mu_eta = TPaveText(55e3,0.64,100e3,0.7)
  mu_d0 = TPaveText(55e3,0.58,100e3,0.64)
  mu_z0 = TPaveText(55e3,0.52,100e3,0.58)
  mumu_m = TPaveText(55e3,0.46,100e3,0.52)
  mumu_q = TPaveText(55e3,0.4,100e3,0.46)
  mu1_trig = TPaveText(55e3,1.03,100e3,1.07)
  vbf_sel = TPaveText(55e3,0.76,100e3,0.82)'''

  title.SetBorderSize(0)
  vbf_cuts.SetBorderSize(0)
  njets.SetBorderSize(0)
  detajj.SetBorderSize(0)
  mjj.SetBorderSize(0)
  mu_sel.SetBorderSize(0)
  mu_pt.SetBorderSize(0)
  mu_eta.SetBorderSize(0)
  mu_d0.SetBorderSize(0)
  mu_z0.SetBorderSize(0)
  mumu_q.SetBorderSize(0)
  mumu_m.SetBorderSize(0)
  mu1_trig.SetBorderSize(0)

  title.SetFillStyle(0)
  title.SetTextAlign(33)
  mu1_trig.SetTextAlign(12)
  
  title.AddText(year+" Data-MC Comparison")
  
  '''vbf_sel.SetTextAlign(12)
  mu_sel.SetTextAlign(12)
  mu_pt.SetTextAlign(12)
  mu_eta.SetTextAlign(12)
  mu_d0.SetTextAlign(12)
  mu_z0.SetTextAlign(12)
  mumu_q.SetTextAlign(12)
  mumu_m.SetTextAlign(12)'''
  
  vbf_cuts.AddText("VBF cuts:")
  njets.AddText("\mathrm{>1jet(pT>30GeV)}")
  detajj.AddText("\mathrm{|\Delta\eta_{jj}>3|}")
  mjj.AddText("\mathrm{m_{jj}>1TeV}")
  mu_sel.AddText("Exactly two muons with:")
  mu_pt.AddText("\mathrm{pT > 30GeV}")
  mu_eta.AddText("|\mathrm{\eta| < 2.4}")
  mu_d0.AddText("|\mathrm{d0(significance)}| < 3.0}")
  mu_z0.AddText("|\mathrm{\Delta z0.sin(\\theta)}| < 0.5}")
  mumu_m.AddText("\mathrm{81GeV \leq m_{\mu\mu} \leq 101GeV}")
  mumu_q.AddText("\mathrm{q_{\mu\mu} = 0}")
  mu1_trig.AddText("Lowest unprescaled single lepton trigger")
  
  yline = TLine(50e3, 1, 280e3, 1)
  yline.SetLineColor(kGray+1)
  yline.SetLineWidth(2)
  yline.SetLineStyle(2)
 
  title.Draw() 
  yline.Draw() 
  legend.Draw() 
  vbf_cuts.Draw()
  njets.Draw()
  detajj.Draw()
  mjj.Draw()
  mu_sel.Draw()
  mu_pt.Draw()
  mu_eta.Draw()
  mu_d0.Draw()
  mu_z0.Draw()
  mumu_m.Draw()
  mumu_q.Draw()
  mu1_trig.Draw()
  aplt.atlas_label(x=0.1, y=0.92, text="Work in Progress")

  titleStr = ""    
  outfile = TFile("output/hTrig/"+year+"/h_trigEff_"+year+"_sysTest.root", "recreate")
  hs_trigEff[0].SetDirectory(outfile)
  hs_trigEff[0].SetNameTitle("h_"+samples[0].lower()+"_"+year, year+" "+samples[0]+titleStr)
  hs_trigEff[1].SetDirectory(outfile)
  hs_trigEff[1].SetNameTitle("h_"+samples[1].lower()+"_"+year, year+" "+samples[1]+titleStr)
  rp = ratioPlot.GetLowerRefGraph()
  rp.SetNameTitle(year+" MET trigger Data-MC Ratio") 
  rp.GetXaxis().SetTitle("\mathrm{E^{T}_{miss} + pT_{\mu\mu}\;(GeV)}")
  outfile.WriteObject(rp, "h_ratioPlot")
  
  # fit error function to ratio curve
  if year != "2015":
    rp = ratioPlot.GetLowerRefGraph()
    mu = rp.GetMean()
    sigma = rp.GetRMS()
    if year == "2016": xmax = 225e3
    if year == "2017": xmax = 300e3
    if year == "2018": xmax = 250e3
    erf = TF1("erf", "TMath::Erf((x-[0])**2/[1])", 100e3, xmax)
    erf.SetParameter(0,mu)
    erf.SetParameter(1,sigma**2)
    print("Parameters before fit:", mu, sigma**2)
    rp.Fit("erf", "R")
    #fit = rp.Fit("erf", "S")
    #parStr0 = str(fit.Parameter(0)) + "±" + str(fit.Error(0))
    #parStr1 = str(fit.Parameter(1)) + "±" + str(fit.Error(1))
    #print("Parameters after fit:", parStr0 + ", " + parStr1)  
    canvas.Update()
    #erf_up = TF1("erf_up", "TMath::Erf((x-[0])**2/[1])", 100e3, xmax)
    #erf_up.SetParameter = (0, fit.Parameter(0) + fit.Error(0))
    #print(fit.Parameter(0) + fit.Error(0))
    #erf_up.SetParameter = (1, fit.Parameter(1) + fit.Error(1))
    #print(fit.Parameter(1) + fit.Error(1))
    #erf_up.Draw()
    #canvas.Update()
    #erf_down = TF1("erf_down", "TMath::Erf((x-[0])**2/[1])", 100e3, xmax)
    #erf_down.SetParameter = (0, fit.Parameter(0) - fit.Error(0))
    #erf_down.SetParameter = (1, fit.Parameter(1) - fit.Error(1))
    
  
  canvas.Print("output/efficiency_plots/MCvsData_metTurnOn_"+year+".png")
 
  titleStr = ""    
  outfile = TFile("output/hTrig/"+year+"/h_trigEff_"+year+"_sysTest.root", "recreate")
  hs_trigEff[0].SetDirectory(outfile)
  hs_trigEff[0].SetNameTitle("h_"+samples[0].lower()+"_"+year, year+" "+samples[0]+titleStr)
  hs_trigEff[1].SetDirectory(outfile)
  hs_trigEff[1].SetNameTitle("h_"+samples[1].lower()+"_"+year, year+" "+samples[1]+titleStr)
  rp = ratioPlot.GetLowerRefGraph()
  rp.SetNameTitle(year+" MET trigger Data-MC Ratio") 
  rp.GetXaxis().SetTitle("\mathrm{E^{T}_{miss} + pT_{\mu\mu}\;(GeV)}")
  outfile.WriteObject(rp, "h_ratioPlot") 
  outfile.Write()
  print("output file written to ", outfile)

def getSF_allYrs():

  outfile = TFile.Open("output/metTrigSF.root", "RECREATE")
  years = ["2016", "2017", "2018"]
  for year in years:
    tfile = TFile.Open("output/hTrig/"+year+"/h_trigEff_"+year+".root")
    h_ratioPlot = tfile.Get("h_ratioPlot")
    outfile.WriteObject(h_ratioPlot, "h_metTrigSF_"+year)
    tfile.Close()
  outfile.Close

parser = argparse.ArgumentParser()
parser.add_argument("--year", type=str)
args = parser.parse_args()

main(args.year)
#getSF_allYrs()
