import matplotlib.pyplot as plt
import csv

def main():
  
  eff, met = [], []
  with open("100GeV_SFs.csv", "r") as infile:
    reader = csv.reader(infile)
    for row in reader:
     met.append(row[0])
     eff.append(row[1])
  infile.close()
  met = [round(float(val), 1) for val in met]
  eff = [round(float(val), 3) for val in eff]
  met.sort()
  eff.sort()
  plt.plot(met, eff)
  plt.savefig("100GeV_SF_withCurve.png") 


main()
