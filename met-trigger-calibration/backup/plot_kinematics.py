from ROOT import TFile, TLegend, TCanvas, gStyle, TH1D, TLine, kRed, TPaveText, TLatex 
import os

def main():

  # open files & get trees
  zjets_tfile = TFile.Open("output/zjets_trigEff.root", "READ") 
  zjets_ttree = zjets_tfile.Get("zEvents_tree")
  #data_tfile = TFile.Open("output/data_run2_trigEff.root", "READ") 
  #data_ttree = data_tfile.Get("zEvents_tree")
  data_tfile = TFile.Open("output/data18-T_trigEff.root", "READ") 
  data_ttree = data_tfile.Get("zEvents_tree")
  
  # histogram setup
  variables = ["met", "proxy_met", "muon_pT", "muon_eta", "muon_phi", "muon_e", "dimuon_mass", "dimuon_pT", "dimuon_dR"]
  xmin = [0, 0, 0, -3, -3.2, 0, 60e3, 0, 0]
  xmax = [400e3, 300, 300e3, 3, 3.2, 300e3, 120e3, 300e3, 4]
  nbins = 50
  legend = TLegend(0.63,0.8,0.9,0.9)
  canvas = TCanvas("c", "c", 10, 10, 800, 500)
  canvas.cd()
  gStyle.SetOptStat(0)
  
  # loop through kinematic variables
  for v_idx in range(len(variables)):
    legend.Clear()
    var = variables[v_idx]
    var_name = var.replace("_", " ")
    h_zjets = TH1D("h_zjets", var_name + " (Z to dimuon selection)", nbins, xmin[v_idx], xmax[v_idx]) 
    h_data = TH1D("h_data", var_name + " (Z to dimuon selection)", nbins, xmin[v_idx], xmax[v_idx]) 
    zjets_ttree.Draw(var+">>h_zjets","","hist") 
    data_ttree.Draw(var+">>h_data",""," hist same") 
    n_zjets = h_zjets.Integral()
    n_data = h_data.Integral()
    h_zjets.Scale(1/n_zjets, "width")
    h_data.Scale(1/n_data, "width")
    h_zjets.SetLineColor(51)
    h_data.SetLineColor(209)
    h_zjets.SetLineWidth(3)
    h_data.SetLineWidth(3)
    ymax = max(h_zjets.GetMaximum(), h_data.GetMaximum())
    h_zjets.GetYaxis().SetRangeUser(0, 1.2*ymax)    
    h_zjets.GetXaxis().SetTitle(var_name)
    legend.AddEntry(h_zjets, "Z+jets: nEvents = " + str(n_zjets))
    legend.AddEntry(h_data, "Data: nEvents = " + str(n_data))
    legend.SetTextSize(0.03)
    legend.Draw()
    canvas.Print("output/kinematic_plots/"+var+".png")
  zjets_tfile.Close()
  data_tfile.Close()


main()
