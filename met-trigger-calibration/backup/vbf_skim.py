# Script takes Zmumu Ts and applies VBF skimming

from ROOT import TH1D, TCanvas, TTree, TFile, TLegend, gPad, gStyle, TLatex, TLorentzVector, TBranch
from array import array
import numpy as np
import os

#wdir = "/Users/s1891044/Documents/Physics/DarkPhotons/source/data17/"
wdir = "/Volumes/Extreme SSD/data16/"

def main(infile, wdir):

  # input and output files
  #outdir = wdir+"vbfskimmed/"
  outdir = wdir
  # input and output trees
  tfile = TFile.Open(wdir+infile, "READ")
  outFile = TFile.Open(outdir+"skimmed_"+infile, "RECREATE")
  # input and output trees
  
  h_numEvents = tfile.Get("numEvents")
  #h_numEvents.SetDirectory(0)
  outFile.WriteTObject(h_numEvents)
  ttree = tfile.Get("T")
  outtree = ttree.CloneTree(0)
  outtree.SetName("miniT")
  outtree.SetDirectory(outFile)
  print("Loop starting, T.GetEntries():", ttree.GetEntries())   
  for entry in range(ttree.GetEntries()):
    if entry > 0 and entry%100000==0: print("Processed {} of {} entries".format(entry,ttree.GetEntries()))
    
    ttree.GetEntry(entry)

    # dijet selection
    jet30_idxs = []
    for j30_idx in range(len(ttree.jet_cal_pt)):
      if ttree.jet_cal_pt.at(j30_idx) < 30e3: continue
      jet30_idxs.append(j30_idx)
    if len(jet30_idxs) < 2: continue
    
    # find leading and subleading jets
    idx_jet1 = jet30_idxs[0]
    for i in range(len(jet30_idxs)):
      if ttree.jet_cal_pt.at(i) > ttree.jet_cal_pt.at(idx_jet1): idx_jet1 = jet30_idxs[i]
    
    idx_jet2 = jet30_idxs[0]
    if idx_jet1 == jet30_idxs[0]: idx_jet2 = jet30_idxs[1]
    for j in range(len(jet30_idxs)):
      idx = jet30_idxs[j]
      if idx != idx_jet1 and (ttree.jet_cal_pt.at(idx) > ttree.jet_cal_pt.at(idx_jet2)): idx_jet2 = idx
   
    j1_p4 = TLorentzVector(0, 0, 0, 0)
    j2_p4 = TLorentzVector(0, 0, 0, 0)
    j1_p4.SetPtEtaPhiE(ttree.jet_cal_pt.at(idx_jet1), ttree.jet_cal_eta.at(idx_jet1), ttree.jet_cal_phi.at(idx_jet1), ttree.jet_cal_e.at(idx_jet1))
    j2_p4.SetPtEtaPhiE(ttree.jet_cal_pt.at(idx_jet2), ttree.jet_cal_eta.at(idx_jet2), ttree.jet_cal_phi.at(idx_jet2), ttree.jet_cal_e.at(idx_jet2))
    
    # apply vbf selection
    mjj = (j1_p4 + j2_p4).M()
    detajj = abs(j1_p4.Eta() - j2_p4.Eta())
    if mjj < 1000e3 and detajj < 3.0: continue

    outtree.Fill()
  
  print("Loop completed, miniT.GetEntries():", outtree.GetEntries())   
  h_numEvents.SetDirectory(0)
  outtree.Write()
  outFile.Close()

for infile in os.listdir(wdir): 
    if 'data16_13TeV.00310872' in infile and '.root' in infile:
      print("Processing ", infile)
      main(infile, wdir)
