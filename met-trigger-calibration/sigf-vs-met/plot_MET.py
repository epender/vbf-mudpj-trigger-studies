from ROOT import TH1D, TCanvas, TTree, TFile, TLegend, gPad, gStyle, TLatex
import atlasplots as aplt

wdir = "/Users/s1891044/Documents/Physics/DarkPhotons/source/v02-00/abcd-samples/"
outdir = "/Users/s1891044/Documents/Physics/DarkPhotons/plot/"
 
f_500758 = TFile.Open(wdir+"frvz_vbf_500758.root", "READ")
f_500761 = TFile.Open(wdir+"frvz_vbf_500761.root", "READ")
f_500762 = TFile.Open(wdir+"frvz_vbf_500762.root", "READ")
 
t_500758 = f_500758.Get("miniT") 
t_500761 = f_500761.Get("miniT") 
t_500762 = f_500762.Get("miniT") 
 
xmin = 0
xmax = 325
xbins = 50

h_500758 = TH1D("h_500758", "VBF Signal: MET Distributions VBF Preselection", xbins, xmin, xmax)
h_500761 = TH1D("h_500761", "h_500761", xbins, xmin, xmax) 
h_500762 = TH1D("h_500762", "h_500762", xbins, xmin, xmax)

vbf_presel = "(scale1fb*intLumi)&&(njet30>1&&mjj>1e6&&abs(detajj)>3)"

canvas = TCanvas("c", "c", 10, 10, 800, 500)
canvas.cd()

gStyle.SetOptStat(0)

h_500758.SetLineColor(209)
h_500761.SetLineColor(63)
h_500762.SetLineColor(51)

t_500758.Draw("MET*0.001>>h_500758", vbf_presel, "hist")
t_500761.Draw("MET*0.001>>h_500761", vbf_presel, "hist same")
t_500762.Draw("MET*0.001>>h_500762", vbf_presel, "hist same")


ymax = h_500758.GetMaximum()
h_500758.GetYaxis().SetRangeUser(0, 1.1*ymax)
h_500758.GetXaxis().SetTitle("MET (GeV)")

legend = TLegend(0.57,0.7,0.9,0.9)
legend.AddEntry(h_500758, "\mbox{m}\mathrm{(\gamma_{d})=0.4GeV}\mbox{, c}\mathrm{\\tau=50mm}")
legend.AddEntry(h_500761, "\mbox{m}\mathrm{(\gamma_{d})=10GeV}\mbox{, c}\mathrm{\\tau=900mm}")
legend.AddEntry(h_500762, "\mbox{m}\mathrm{(\gamma_{d})=15GeV}\mbox{, c}\mathrm{\\tau=1000mm}")
legend.SetTextSize(0.03)
legend.Draw("hist same") 
aplt.atlas_label(x=0.14, y=0.92, text="Internal");

canvas.Print("/Users/s1891044/Documents/Physics/DarkPhotons/met-trigger-calibration/signal_MET.png")
