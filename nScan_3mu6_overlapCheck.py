from ROOT import TFile, TTree
import csv

inputDir = '/Volumes/Extreme SSD/v02-00/miniT/'
files = []

# NEED TO READ FILES IN ORDER
names = ["521257", "521258", "500757", "500758", "521259", "521260", "521261", "500761", "500762", "521262", "521263"]
files = ["frvz_vbf_"+name+".root" for name in names]

print(type(files))

trimuon_total = []
nscan_total = []
both_total = []
for vbffile in files:
  trimuon_count = 0
  nscan_count = 0
  both_count = 0
  tfile = TFile(inputDir+vbffile)
  ttree = tfile.Get("miniT")
  for entry in range(ttree.GetEntries()):
    trimuon_pass = False
    nscan_pass = False
    both_pass = False
    ttree.GetEntry(entry)
    if ttree.LJ1_type != 0: continue
    weight = ttree.scale1fb*ttree.intLumi
    if ttree.trig.at(22): trimuon_pass = True
    if ttree.RunNumber in range(267069, 284669)and ttree.trig.at(30): nscan_pass = True # 2015
    if ttree.RunNumber in range(296938, 304495) and ttree.trig.at(31): nscan_pass = True # 2016a
    if ttree.RunNumber in range(305114, 311564) and ttree.trig.at(32): nscan_pass = True # 2016b
    if ttree.RunNumber in range(324317, 364486) and (ttree.trig.at(33) or ttree.trig.at(34)): nscan_pass = True # 2017/18
    if nscan_pass and trimuon_pass: both_pass = True
    if trimuon_pass: trimuon_count+=weight
    if nscan_pass: nscan_count+=weight
    if both_pass: both_count+=weight
  print("3mu6, nScan, both: ", trimuon_count, nscan_count, both_count, " for file "+vbffile)
  trimuon_total.append(trimuon_count)
  nscan_total.append(nscan_count)
  both_total.append(both_count)

print("nFiles, nEntries: ", len(files), len(trimuon_total))

masses = ['0.017', '0.05', '0.1', '0.4', '0.9', '2', '6', '10', '15', '25', '40']
with open("overlapCheck.csv", "w") as outfile:
  # write header
  outfile.write('Trigger, ')
  for mass in masses: 
    outfile.write('m(yd)='+mass)
    if mass != masses[-1]: outfile.write(', ')
    if mass == masses[-1]: outfile.write('\n')
  # write body
  outfile.write('3mu6_msonly, ')
  for n_3mu6 in trimuon_total: 
    outfile.write(str(round(n_3mu6, 2)))
    if n_3mu6 != trimuon_total[-1]: outfile.write(', ')
    if n_3mu6 == trimuon_total[-1]: outfile.write('\n')
  outfile.write('nScan, ')
  for n_nScan in nscan_total: 
    outfile.write(str(round(n_nScan, 2)))
    if n_nScan != nscan_total[-1]: outfile.write(', ')
    if n_nScan == nscan_total[-1]: outfile.write('\n')
  outfile.write('3mu6||nScan, ')
  for n_both in both_total: 
    outfile.write(str(round(n_both, 2)))
    if n_both != both_total[1]: outfile.write(', ')
    if n_both == both_total[1]: outfile.write('\n')  
    
