# -*- coding: latin-1 -*-#
# Script containing helper functions for trigger efficiency framework
# Emily Pender 16/11/2023

from ROOT import TLorentzVector
import math

def get_fileStr(self, fileName):
  if '500758' in fileName: fileStr = '\\mbox{VBF Signal: m(}\mathrm{\gamma d}\\mbox{)=0.4GeV, c}\mathrm{\\tau}\\mbox{=50mm}'
  if '500757' in fileName: fileStr = '\\mbox{VBF Signal: m(}\mathrm{\gamma d}\\mbox{)=0.1GeV, c}\mathrm{\\tau}\\mbox{=15mm}'
  return(fileStr)  
 
def find_lxy(self, index):
  lx = self.tree.truthDecayVtx_x.at(index)
  ly = self.tree.truthDecayVtx_y.at(index)
  lxy = (lx*lx + ly*ly)**0.5
  return lxy

def find_ctau(self, index):
  lxyz = math.sqrt((find_lxy(self, index)**2)+(self.tree.truthDecayVtx_z.at(index)**2))
  yd_p4 = TLorentzVector(0, 0, 0, 0)
  yd_p4.SetPtEtaPhiE(self.tree.truthPt.at(index), self.tree.truthEta.at(index), self.tree.truthPhi.at(index), self.tree.truthE.at(index))
  ctau = lxyz/(yd_p4.Gamma()*yd_p4.Beta())
  return ctau

def find_DP(self):
  nPart = self.tree.truthPdgId.size()
  dp_collection = []
  lead_DPidx = None
  for index in range(nPart):
    # require decay within ATLAS
    # save only if dark photon that decays to muons
    if self.tree.truthPdgId[index] != 3000001 or find_lxy(self, index) > self.endATLAS: continue
    if self.decay == 'muonic' and self.tree.truthDecayType[index] != 13: continue
    if self.decay == 'calo' and self.tree.truthDecayType[index] == 13: continue
    DP_pt = self.tree.truthPt.at(index)
    if 'Lxy' in self.var or 'CalRatio' in self.trigger:
      if abs(self.tree.truthEta.at(index)) > 1.1: continue # select decays in the barrel 
      dp_collection.append([index, DP_pt]) 
    elif 'Lz' in self.var:
      if abs(self.tree.truthEta.at(index)) < 1.1: continue # select decays in the endcap
      dp_collection.append([index, DP_pt]) 
    else: dp_collection.append([index, DP_pt])
  # find leading DP index
  pTsorted = sorted(dp_collection, key=lambda x: x[1])
  if len(pTsorted)>0: lead_DPidx = (pTsorted[-1])[0]
  return lead_DPidx

def get_value(self, lead_DPidx):
  # find value of x variable of leading DP 
  if 'Lxy' in self.var: value = find_lxy(self, lead_DPidx)
  elif 'Lz' in self.var: value = self.tree.truthDecayVtx_z.at(lead_DPidx)
  elif 'ctau' in self.var: value = find_ctau(self, lead_DPidx)
  else:
      attr = getattr(getattr(self.tree, 'truth'+self.var), 'at')
      value = attr(lead_DPidx)
      value = value*0.001 # get pT into GeV
  return value

def fill_hists(self, pass_list, value):
  if self.tree.RunNumber in range(267069, 284669): #2015
     self.hs_noTrig[0].Fill(value)
     if pass_list[0] == 1: self.hs_trig[0].Fill(value)
  if self.tree.RunNumber in range(296938, 311563): #2016
     self.hs_noTrig[1].Fill(value)
     if pass_list[1] == 1: self.hs_trig[1].Fill(value)
  if self.tree.RunNumber in range(324317, 341649): #2017
     self.hs_noTrig[2].Fill(value)
     if pass_list[2] == 1: self.hs_trig[2].Fill(value)
  if self.tree.RunNumber in range(348154, 364485): #2018
     self.hs_noTrig[3].Fill(value)
     if pass_list[3] == 1: self.hs_trig[3].Fill(value)

def fill_vbf_hists(self, pass_list):
  if self.tree.RunNumber in range(324317, 341649): #2017
     self.hs_noTrig[0].Fill(value)
     if pass_list[0] == 1: self.hs_trig[0].Fill(value)
  if self.tree.RunNumber in range(348154, 364485): #2018
     self.hs_noTrig[1].Fill(value)
     if pass_list[1] == 1: self.hs_trig[1].Fill(value)
