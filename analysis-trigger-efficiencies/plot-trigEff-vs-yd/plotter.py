# -*- coding: latin-1 -*-#
# Script to plotter trigger efficiency as a function truth dark photon variable
# Emily Pender 16/11/23

import ROOT
from ROOT import TCanvas, TLine, kBlue, TLegend, gStyle, TPaveText, TLatex 
import atlasplots as aplt

aplt.set_atlas_style()

def plot(self, fileStr):

  if self.decay == "muonic": decayStr = "#gamma_{d} #rightarrow #mu^{+} #mu^{-}"
  if self.decay == "calo": decayStr = "#gamma_{d} #rightarrow e^{+}e^{-}/q#bar{q}"
  
  # set variable string and units for axis title
  if self.var == "ctau": varStr = "c#tau "+decayStr
  elif self.var == "Pt": varStr = "pT "+decayStr
  else: varStr = self.var + " " + decayStr

  if "Lxy" in self.var or "Lz" in self.var or "ctau" in self.var in self.var: units = ' [mm]'
  elif "Pt" in self.var: units = ' [GeV]'
  
  # set colours
  if self.trigger != "VBF": colors = [99, 92, 8, 64]
  else: colors = [8, 64]
  #if self.decay == 'muonic': colors = [100, 92, 8, 64]
  #if self.decay == 'calo': colors = [65, 209, 53]
  
  # initialize canvas and legend
  canvas = TCanvas("canvas")
  gStyle.SetOptStat(0)
  canvas.cd()
  canvas.SetRightMargin(0.15)
  canvas.SetLeftMargin(0.15)
  canvas.SetTopMargin(0.1)

  if self.trigger == "NarrowScan" and self.var == "Lxy": x0, x1, y0, y1 = 0.8, 0.7, 0.85, 0.7
  if self.trigger == "NarrowScan" and self.var == "Pt": x0, x1, y0, y1 = 0.8, 0.7, 0.85, 0.7
  if self.trigger == "MET" and self.var == "Lxy" and self.decay == "muonic": x0, x1, y0, y1 = 0.8, 0.7, 0.85, 0.7
  if self.trigger == "MET" and self.var == "Lxy" and self.decay == "calo": x0, x1, y0, y1 = 0.8, 0.7, 0.4, 0.25
  if self.trigger == "MET" and self.var == "Pt": x0, x1, y0, y1 = 0.8, 0.7, 0.4, 0.25
  if self.trigger == "CalRatio" and self.var == "Pt": x0, x1, y0, y1 = 0.8, 0.7, 0.85, 0.7
  if self.trigger == "CalRatio" and self.var == "Lxy": x0, x1, y0, y1 = 0.8, 0.7, 0.85, 0.7
  legend = TLegend(x0,y0,x1,y1)
  legend.SetBorderSize(0)
  legend.SetTextFont(42)
  legend.SetTextSize(0.045)
  legend.SetFillColor(0)
  legend.SetFillStyle(0)
  legend.SetLineColor(0)

  # create efficiency histograms 
  hs_trigEff = []
  #lastbin = self.hs_noTrig[0].GetNbinsX()
  #overflow = self.hs_noTrig[0].GetBinContent(lastbin+1)
  #content_lastbin = self.hs_noTrig[0].GetBinContent(lastbin)
  for hist_idx in range(len(self.hs_trig)):
    #self.hs_noTrig[hist_idx].SetBinContent(lastbin, content_lastbin + overflow)
    #self.hs_trig[hist_idx].SetXTitle(varStr + units)
    #self.hs_noTrig[hist_idx].SetXTitle(varStr + units)
    #self.hs_trig[hist_idx].Sumw2()
    #self.hs_noTrig[hist_idx].Sumw2()
    # create recoEff hist for each sample
    h_trigEff_temp = self.hs_trig[hist_idx].Clone()
    h_trigEff_temp.SetDirectory(0)
    h_trigEff_temp.Divide(h_trigEff_temp, self.hs_noTrig[hist_idx], 1,  1, "B")
    #h_trigEff_temp.Sumw2()
    hs_trigEff.append(h_trigEff_temp)
    legend.AddEntry(self.hs_noTrig[hist_idx], self.years[hist_idx] + " all events", 'l')
    legend.AddEntry(self.hs_trig[hist_idx], self.years[hist_idx] + " events passing trigger" , 'l')
    self.hs_noTrig[hist_idx].SetLineColor(colors[hist_idx])
    if hist_idx == 0:
      self.hs_noTrig[0].SetMinimum(0.5)
      self.hs_noTrig[0].Draw("P")
    else:
      self.hs_noTrig[hist_idx].Draw("SAME P")

  legend.Draw("same")
  #canvas.Print(self.outDir + "DP_" + self.var + '_' + self.year + 'trigVsAll.png')
  canvas.Clear()
  legend.Clear()

  # plot all events 
  for idx in range(len(self.hs_noTrig)):
    if idx == 0: self.hs_noTrig[idx].Draw()
    else: self.hs_noTrig[idx].Draw("SAME")
  #canvas.Print(self.outDir + 'DP_' + self.var + '_noTrig.png')
  canvas.Clear()

  # plot events passing trigger
  for idx in range(len(self.hs_trig)):
    if idx == 0: self.hs_trig[idx].Draw()
    else: self.hs_trig[idx].Draw("SAME")
  #canvas.Print(self.outDir + 'DP_' + self.var + '_' + self.year + 'Trig.png')
  canvas.Clear()

  # plot trigger efficiency
  for idx in range(len(hs_trigEff)):
    legend.AddEntry(hs_trigEff[idx], self.years[idx], 'l')
    hs_trigEff[idx].SetLineColor(colors[idx])
    hs_trigEff[idx].SetLineWidth(2)
    #hs_trigEff[idx].GetYaxis().SetTitleSize(0.045)
    #hs_trigEff[idx].GetXaxis().SetTitleSize(0.04)
    hs_trigEff[idx].GetYaxis().SetTitleSize(22)     # to use atlasplots
    hs_trigEff[idx].GetXaxis().SetTitleSize(22)     # to use atlasplots
    hs_trigEff[idx].GetYaxis().SetLabelSize(22)     # to use atlasplots
    hs_trigEff[idx].GetXaxis().SetLabelSize(22)     # to use atlasplots
    hs_trigEff[idx].SetMaximum(1.2)
    hs_trigEff[idx].SetMinimum(0)
    hs_trigEff[idx].SetMinimum(0)
    hs_trigEff[idx].SetMarkerStyle(20+idx)
    hs_trigEff[idx].SetMarkerColor(colors[idx])
    if idx == 0:
      hs_trigEff[idx].SetXTitle(varStr + units)
      hs_trigEff[idx].SetYTitle("Efficiency")
      hs_trigEff[idx].SetTitle("") 
      #hs_trigEff[idx].SetTitle("\\mbox{"+yearStr+" Signal Trigger Efficiency vs. }\mathrm{\\gamma d(}"+varStr+"\\mbox{)}") 
      #hs_trigEff[idx].SetTitleSize(0.5) # x axis label will disappear if this is set... 
      hs_trigEff[idx].Draw("E" )
    else: hs_trigEff[idx].Draw("E SAME")

  #if self.var == 'ctau': sample = TPaveText(100,1.11,199,1.16) 
  #if self.var == 'Lxy' and self.decay == 'muonic': sample = TPaveText(5e3,1.02,9990,1.07)
  #if self.var == 'Lxy' and self.decay == 'calo': sample = TPaveText(500,1.02,6.7e3,1.07) 
  #if self.var == 'Lz' and self.decay == 'muonic': sample = TPaveText(7e3,1.02,13990,1.07)
  #if self.var == 'Lz' and self.decay == 'calo': sample = TPaveText(1e3,1.02,1e4,1.07)
  #if self.var =='Pt':sample = TPaveText(150,1.02,299,1.07)

  aplt.atlas_label(x=0.18, y=0.92, text="Simulation")
  hs_trigEff[idx].SetXTitle(varStr + units)
  hs_trigEff[idx].SetYTitle("Efficiency")

  
  if self.decay == 'muonic': sample_name = "VBF Signal: m_{#gamma_{d}}= 0.4 GeV, c#tau_{#gamma_{d}}= 50mm"
  if self.decay == 'calo': sample_name = "VBF Signal: m_{#gamma_{d}}= 0.1 GeV, c#tau_{#gamma_{d}}= 15mm"
  sample = TLatex(0.18, 0.83, sample_name)
  sample.SetNDC(True)
  sample.SetTextSize(20)
  '''if self.decay == 'muonic': sample_name = "VBF Signal: m_{#gamma_{d}}= 0.4 GeV, c#tau_{#gamma_{d}}= 50mm"
  if self.decay == 'calo': sample_name = "VBF Signal: m_{#gamma_{d}}= 0.1 GeV, c#tau_{#gamma_{d}}= 15mm"
  if self.trigger == "NarrowScan" and self.var == "Pt": sample = TPaveText(25, 1.05, 200, 1.15)
  if self.trigger == "NarrowScan" and self.var == "Lxy": sample = TPaveText(500, 1.05, 10000, 1.15)
  if self.trigger == "MET" and self.var == "Pt": sample = TPaveText(25,1.05, 200, 1.15)
  if self.trigger == "MET" and self.var == "Lxy": sample = TPaveText(500, 1.05, 8000, 1.15)
  sample.AddText(sample_name)
  sample.SetTextSize(0.04)
  sample.SetBorderSize(0)
  sample.SetTextColor(1)
  sample.SetFillColor(0)
  sample.SetTextAlign(11)
  #sample = TLatex(0.19, 0.84, sample_name)
  #sample.SetNDC(True)
  #sample.SetTextSize(22)'''
  sample.Draw()

  if self.var == "Lxy": cutStr = "; |#eta|<1.0"
  if self.var == "Lz": cutStr = "; |#eta|>1.1"
  if self.var == "Pt": cutStr = ""
  cuts = TLatex(0.19, 0.74, decayStr+cutStr)
  cuts.SetNDC(True)
  cuts.SetTextSize(20)
  #cuts.Draw()

  #if self.decay == 'muonic': sample.AddText("\\mbox{VBF Signal: }\\mathrm{m_{\\gamma_d}=0.4GeV,\\;c\\tau_{\\gamma_d}=50mm}")
  #if self.decay == 'calo': sample.AddText("\\mbox{VBF Signal: }\\mathrm{m_{\\gamma_d}=0.1GeV,\\;c\\tau_{\\gamma_d}=15mm}")
  
  #sample.SetBorderSize(0)
  #sample.SetTextSize(0.04)
  #sample.SetTextColor(2)
  #sample.SetFillColor(0)
  #sample.SetTextAlign(32) 
  #sample.Draw("same")
  
  #if "Lz" or "Lxy" in self.var: x_title = self.var+" [mm]" 
  #if "Pt" in self.var: x_title = self.var+" [pT]" 
  #hs_trigEff[-1].SetXTitle(x_title)
  #hs_trigEff[-1].SetYTitle(self.trigger+" Trigger Efficiency")

  if len(hs_trigEff) > 1: legend.Draw("same") 
  #if 'lxy' in self.var: start = 4e3
  #elif 'lz' in self.var: start = 7e3
  # if plotted as a function of decay length, add lines representing MS
  '''if 'lxy' in self.var or 'lz' in self.var:
    line_MS = TLine(start,0.,start, yMax)
    line_MS.SetLineColor(kBlue)
    line_MS.SetLineWidth(2)
    line_MS.Draw()
    line_MS_2 = TLine(self.endATLAS,0.,self.endATLAS, yMax)
    line_MS_2.SetLineColor(kBlue)
    line_MS_2.SetLineWidth(2)
    line_MS_2.Draw()'''
  canvas.Print(self.outDir + 'yd_' + self.var + "_" + self.trigger + '_trigEff_' + self.decay + '.png')
