# -*- coding: latin-1 -*-#
# Program witten by Emily Pender, last updated on 16/11/23 
# Trigger efficiency calculated as a function of a given leading dark photon parameter TRUTH LEVEL INFO 
# DP Parameter and parameter range, trigger type and year of implementaation specified in command line by user
# 1. Plot trigger efficiency of one sample for one channel or
# 2. Compare efficiencies between years

# TO RUN: 
# python trigEff.py --trigger NarrowScan--var Pt --decay muonic

from ROOT import TFile, TLatex, TH1D
import numpy, math, os
import multiprocessing as mp
import argparse
import utils
import plotter
import itertools

class Eff:

  def __init__(self, trigger, var, decay):

    # arguments
    self.var = var
    self.decay = decay
    self.trigger = trigger
    
    self.tree = None
    self.years = []
    self.endATLAS = 2e5

    # histograms
    self.hs_trig = []
    self.hs_noTrig = []
    self.hs_trigEff = []
    self.outDir="output/"
  
  def nScan_presel(self, lead_DPidx):

    # want ALL events, numerator and denominator, to pass this presel!!
    presel = False
    leadDP_muPt = []
    for child_idx in range(self.tree.childPt.size()):
      child_pt = self.tree.childPt.at(child_idx)
      if self.tree.childMomBarcode[child_idx] == self.tree.truthBarcode[lead_DPidx]: leadDP_muPt.append([child_idx, child_pt])
    mu_pTsorted = sorted(leadDP_muPt, key=(lambda x:x[1]))
    mu1_pT = (mu_pTsorted[1])[1]
    mu2_pT = (mu_pTsorted[0])[1]
    if (mu1_pT < 2e4) or (mu2_pT < 6e4): presel = True

    return presel

  def nScan(self):

    pass15, pass16, pass17, pass18 = False, False, False, False
    if self.tree.RunNumber in range(267069, 284669) and self.tree.trig.at(30): pass15 = True #2015
    if self.tree.RunNumber in range(296938, 304495) and self.tree.trig.at(31): pass16 = True #2016 A-F3
    if self.tree.RunNumber in range(305114, 311564) and self.tree.trig.at(32): pass16 = True #2016 G1-end
    if (self.tree.RunNumber in range(324317, 341650) or self.tree.RunNumber in range(348154, 364486)) and (self.tree.trig.at(33) or self.tree.trig.at(34) or self.tree.trig.at(35)): pass18, pass17 = True, True #2017/2018
 
    return [pass15, pass16, pass17, pass18]

  def MET(self):
    
    pass15, pass16, pass17, pass18 = False, False, False, False
    if self.tree.RunNumber in range(267069, 284669) and self.tree.trig.at(28): pass15 = True #2015
    if self.tree.RunNumber in range(296939, 302873) and self.tree.trig.at(27): pass16 = True #2016
    if self.tree.RunNumber in range(302872, 311482) and self.tree.trig.at(26): pass16 = True #2016 
    if self.tree.RunNumber in range(324320, 341650) and self.tree.trig.at(25): pass17 = True #2017
    if self.tree.RunNumber in range(348197, 365586) and self.tree.trig.at(24): pass18 = True #2018
 
    return [pass15, pass16, pass17, pass18]
 
  def trimuon(self):

    pass15, pass16, pass17, pass18 = False, False, False, False
    if self.tree.trig.at(22):
     if self.tree.RunNumber in range(267069, 284669): pass15 = True #2015
     if self.tree.RunNumber in range(296939, 311564): pass16 = True #2016
     if self.tree.RunNumber in range(324317, 341650): pass17 = True #2017
     if self.tree.RunNumber in range(348197, 365586): pass17 = True #2018
    
    return [pass15, pass16, pass17, pass18]

  def calRatio(self):
    late = [39, 40, 46, 47]
    pass15, pass16, pass17, pass18 = False, False, False, False
    if self.tree.RunNumber in range(267069, 284669) and (self.tree.trig.at(38) or self.tree.trig.at(43)): pass15 = True #2015
    if self.tree.RunNumber in range(296938, 311564) and (self.tree.trig.at(39) or self.tree.trig.at(40)): pass16 = True #2016
    if self.tree.RunNumber in range(324317, 341649) and any([self.tree.trig.at(i) for i in late]): pass17 = True #2017
    if self.tree.RunNumber in range(348154, 364486) and any([self.tree.trig.at(i) for i in late]): pass18 = True #2018
    
    return [pass15, pass16, pass17, pass18]

  #def VBF(self, lead_DPidx):
  

  def setup(self):
    
    inputDir = "/Volumes/ExtremeSSD/doSys/"
    if not os.path.isdir(inputDir):
        print(inputDir + " does not exist, exitting")
        exit()
    if not os.path.isdir(self.outDir):
        os.makedirs(self.outDir)
    print("output directory set to be " + self.outDir)
    tree = "T"
    # access each root file in the inputDir
    # apply cuts on the tree and save the output in the outDir
    files = []
    outputfiles = []
    for fileName in os.listdir(inputDir):
        if self.decay == 'muonic' and '500758' not in fileName: continue
        if self.decay == 'calo' and '500757' not in fileName: continue
        files.append(inputDir + fileName)
        outputfiles.append(self.outDir + fileName)
    pool = mp.Pool(len(files))
    print("files: ",files)
    results = [pool.apply_async(main, (files[i], outputfiles[i], tree, i)) for i in range(len(files))]
    pool.close()
    pool.join()
    return files
  
  def main(self):
    
    if self.trigger not in ["NarrowScan", "Trimuon", "MET", "CalRatio", "VBF"]: 
       print("Trigger name not recognised, please try again")
       exit()

    if self.trigger == "CalRatio" and self.var == "Lxy": xmin, xmax = 0, 5e3
    else:
      if self.var == "ctau": xmin, xmax = 0, 200 
      if self.var == "Lxy": xmin, xmax = 0, 12e3 
      if self.var == "Lz": xmin, xmax = 0, 15e3 
      if self.var == "Pt": xmin, xmax = 20, 250 
    
    if self.trigger == "VBF": self.years = ["2017", "2018"]
    else: self.years = ["2015", "2016", "2017", "2018"]
    
    # (re)set hist lists
    self.hs_trig, self.hs_noTrig = [], []
    if self.trigger == "CalRatio": self.endATLAS = 5e2
    if 'Lxy' in self.var: self.endATLAS = 12e3
    elif 'Lz' in self.var: self.endATLAS = 14e3
    
    fileStr = self.compare_years(xmin, xmax)
         
    # once hist for all samples have been created,
    # plot on same: canvas
    plotter.plot(self, fileStr)
 
  def compare_years(self, xmin, xmax):
    
    nbins = 50
    fileName = self.setup()[0]
    fileStr = utils.get_fileStr(self, fileName)
    print("self.years: ", self.trigger)
    # append histogram for each year 
    for year in range(len(self.years)):
      h_noTrig_temp = TH1D("noTrig vs. yd " + self.var, "noTrig vs. yd " + self.var, nbins, xmin, xmax)
      #h_noTrig_temp.Sumw2()
      h_noTrig_temp.SetDirectory(0)
      self.hs_noTrig.append(h_noTrig_temp)
      h_trig_temp = TH1D(self.trigger + " Trigger Efficiency vs. yd " + self.var, "Trigger Efficiencies vs. yd " + self.var, nbins, xmin, xmax)
      #h_trig_temp.Sumw2()
      h_trig_temp.SetDirectory(0)
      if "Lxy" or "Lz" in self.var: h_trig_temp.SetXTitle(self.var +' [mm]')
      elif "Pt" in self.var: h_trig_temp.SetXTitle(self.var +' [GeV]')
      self.hs_trig.append(h_trig_temp)
    tfile = TFile(fileName)
    self.tree = tfile.Get("T")
    # perform event loop
    count=0 # for testing
    for entry in range(self.tree.GetEntries()):  
      self.tree.GetEntry(entry)
      #if count > 10: continue
      #count+=1
      lead_DPidx = utils.find_DP(self)
      if not lead_DPidx: continue
      value = utils.get_value(self, lead_DPidx)
      #print(value) 
      if self.trigger == "NarrowScan":
        if not self.nScan_presel(lead_DPidx): continue
        pass_list = self.nScan()
        utils.fill_hists(self, pass_list, value)
      if self.trigger == "MET":
        #if self.tree.MET < 225e3: continue
        pass_list = self.MET() 
        utils.fill_hists(self, pass_list, value)
      if self.trigger == "Trimuon": 
        pass_list = self.trimuon() 
        utils.fill_hists(self, pass_list, value)
      if self.trigger == "CalRatio":
        if utils.find_lxy(self, lead_DPidx) > 5e3: continue
        pass_list = self.calRatio() 
        utils.fill_hists(self, pass_list, value)
      # separate case for VBF since only online in 2018
      if self.trigger == "VBF": 
        pass_list = self.vbf() 
        utils.fill_vbf_hists(self, pass_list, value)
    tfile.Close()
    return fileStr 

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("--trigger", help="Trigger to plot efficiency of", type=str)   
    parser.add_argument("--var", help="yd truth variable to plot efficiency wrt.", type=str)   
    parser.add_argument("--decay", help="Decay mode of truth dark photon", type=str)   
    args = parser.parse_args() 
    trigEff = Eff(args.trigger, args.var, args.decay)
    trigEff.main()

if __name__ == '__main__': main()

