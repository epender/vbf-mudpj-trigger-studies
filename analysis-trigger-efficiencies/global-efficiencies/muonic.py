# Program written by Emily Pender (last edited 21/10/22)
# Compare trigger efficiency after muonic SR selection
# 1. metTrig, 3mu6, nScan and OR
# 2. metTrig, VBFtrig and OR
#---------------------------------------
# muDPJ trigger: 3mu6||nScan 
# nScan trigger: any lowest unprescaled 
# MET trigger: any lowest unprescaled
# VBF trigger: inclusive
#---------------------------------------


import ROOT
import numpy as np
import csv

def main():
  
  # input/output info
  indir = '/Users/s1891044/Documents/Physics/DarkPhotons/source/v02-00/abcd-samples/'
  outdir = '/Users/s1891044/Documents/Physics/DarkPhotons/vbf-mudpj-trigger-studies/analysis-trigger-efficiencies/global-efficiencies/output/root/'
  outfile = ROOT.TFile.Open(outdir+"muonic_noPresel_out.root", "RECREATE")
  vbffiles = ['frvz_vbf_500758.root', 
	      'frvz_vbf_500759.root', 
              'frvz_vbf_500760.root', 
              'frvz_vbf_500761.root', 
              'frvz_vbf_500762.root']

  # save yields and efficiencies per sample in 2D histogram
  h_trigEff = ROOT.TH2D("h_trigEff", "h_trigEff", 22, 0, 22, len(vbffiles), 0, len(vbffiles)) 
  h_trigEff.Sumw2() 
  # loop over y axis
  print("\n")
  for inFile in vbffiles:
     print("RUNNING ON SAMPLE ", inFile, "...")
     print("----------------------------------------")
     print("\n")
     tfile = ROOT.TFile(indir+inFile)
     vbffile = (inFile.split('_')[2]).split('.')[0]
     ttree = tfile.Get("miniT")
     # initialise counters
     count = 0                                   # unscaled event count for testing 
     # loop through signal MC tree 
     for entry in range(ttree.GetEntries()):
       #if count > 100: continue
       count+=1
       ttree.GetEntry(entry)
       weight = ttree.scale1fb*ttree.intLumi
       # apply signal region selection 
       # total_SRcounts = SR(ttree, total_SRcounts)
       '''if (ttree.njet30<2) or (ttree.mjj<1e6) or (abs(ttree.detajj)<3): continue     # VBF filter
       if (ttree.nLJmus20<1): continue                                               # nmuDPJ selection
       if (ttree.LJmu1_isGood!=1): continue                                          # muDPJ quality cuts
       if (ttree.MET<80e3): continue                                                 # MET cut
       if (ttree.neleSignal!=0) or (ttree.nmuSignal!=0): continue                    # lepton veto 
       if (ttree.hasBjet!=0): continue                                               # b-jet veto
       if (abs(ttree.dphijj)>2.5): continue                                          # |dphijj| cut
       if (ttree.LJmu1_centrality<0.7): continue                                     # muDPJ centrality
       if (ttree.LJmu1_isoID>4.5e3) or (abs(ttree.LJmu1_charge)!=0): continue        # muDPJ ABCD plane SR selection'''
       # fill denominator
       h_trigEff.Fill("run2_noTrig", vbffile, weight)		                   
       h_trigEff.Fill("run2_noTrig_unscaled", vbffile,  1)
       # test if event passes triggers for full run 2
       run2_triggers(ttree, h_trigEff, vbffile, weight)
       # continue only if event is from 2018 period k (when vbf triggers are on)
       if (ttree.RunNumber < 355529): continue
       h_trigEff.Fill("2018k_noTrig", vbffile, weight)
       h_trigEff.Fill("2018k_noTrig_unscaled", vbffile, 1)
       # test if event passes triggers for 2018k only
       vbfPass = vbf_offline(ttree)
       muDPJvsVBF(ttree, h_trigEff, vbffile, weight, vbfPass)

     ybin = vbffiles.index(inFile)+1
     # get yields from TH2 and print
     run2_noTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("run2_noTrig"), ybin)
     #run2_muTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("run2_muTrig"), ybin)
     run2_3mu6Trig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("run2_3mu6Trig"), ybin)
     run2_nScanTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("run2_nScanTrig"), ybin)
     run2_metTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("run2_metTrig"), ybin)
     run2_orTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("run2_orTrig"), ybin)
     vbf2018k_noTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("2018k_noTrig"), ybin)
     vbf2018k_muTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("2018k_muTrig"), ybin)
     vbf2018k_vbfTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("2018k_vbfTrig"), ybin)
     vbf2018k_orTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("2018k_orTrig"), ybin)

     run2_noTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("run2_noTrig"), ybin)
     #run2_muTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("run2_muTrig"), ybin)
     run2_3mu6Trig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("run2_3mu6Trig"), ybin)
     run2_nScanTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("run2_nScanTrig"), ybin)
     run2_metTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("run2_metTrig"), ybin)
     run2_orTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("run2_orTrig"), ybin)
     vbf2018k_noTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("2018k_noTrig"), ybin)
     vbf2018k_muTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("2018k_muTrig"), ybin)
     vbf2018k_vbfTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("2018k_vbfTrig"), ybin)
     vbf2018k_orTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("2018k_orTrig"), ybin)

     print("run2_noTrig: ", str(round(run2_noTrig, 4)), "±", str(round(run2_noTrig_err, 4)))   
     #print("run2_muTrig: ", str(round(run2_muTrig, 4)), "±", str(round(run2_muTrig_err, 4)))
     print("run2_3mu6Trig: ", str(round(run2_3mu6Trig, 4)), "±", str(round(run2_3mu6Trig_err, 4)))
     print("run2_nScanTrig: ", str(round(run2_nScanTrig, 4)), "±", str(round(run2_nScanTrig_err, 4)))
     print("run2_metTrig: ", str(round(run2_metTrig, 4)), "±", str(round(run2_metTrig_err, 4)))
     print("run2_orTrig: ", str(round(run2_orTrig, 4)), "±", str(round(run2_orTrig_err, 4)))
     print("2018k_noTrig: ", str(round(vbf2018k_noTrig, 4)), "±", str(round(vbf2018k_noTrig_err, 4)))  
     print("2018k_muTrig: ", str(round(vbf2018k_muTrig, 4)), "±", str(round(vbf2018k_muTrig_err, 4)))
     print("2018k_vbfTrig: ", str(round(vbf2018k_vbfTrig, 4)), "±", str(round(vbf2018k_vbfTrig_err, 4)))
     print("2018k_orTrig: ", str(round(vbf2018k_orTrig, 4)), "±", str(round(vbf2018k_noTrig_err, 4)))
     print("\n")
     
     # calculate trigger efficiencies, fill histogram and print
     #run2_muEff = (run2_muTrig/run2_noTrig)      
     run2_3mu6Eff = (run2_3mu6Trig/run2_noTrig)      
     run2_nScanEff = (run2_nScanTrig/run2_noTrig)      
     run2_metEff = (run2_metTrig/run2_noTrig)      
     run2_orEff = (run2_orTrig/run2_noTrig)      
     vbf2018k_muEff = (vbf2018k_muTrig/vbf2018k_noTrig)      
     vbf2018k_vbfEff = (vbf2018k_vbfTrig/vbf2018k_noTrig)      
     vbf2018k_orEff = (vbf2018k_orTrig/vbf2018k_noTrig)      
      
     #h_trigEff.Fill("run2_muTrig_eff", vbffile, run2_muEff)      
     h_trigEff.Fill("run2_3mu6Trig_eff", vbffile, run2_3mu6Eff) 
     h_trigEff.Fill("run2_nScanTrig_eff", vbffile, run2_nScanEff)      
     h_trigEff.Fill("run2_METTrig_eff", vbffile, run2_metEff)      
     h_trigEff.Fill("run2_ORTrig_eff", vbffile, run2_orEff)      
     h_trigEff.Fill("2018k_muTrig_eff", vbffile, vbf2018k_muEff)      
     h_trigEff.Fill("2018k_vbfTrig_eff", vbffile, vbf2018k_vbfEff)      
     h_trigEff.Fill("2018k_ORTrig_eff", vbffile, vbf2018k_orEff)      
   
     #print("run2_muEff: ", str(round(run2_muEff, 4)))
     print("run2_3mu6Eff: ", str(round(run2_3mu6Eff, 4)))
     print("run2_nScanEff: ", str(round(run2_nScanEff, 4)))
     print("run2_metEff: ", str(round(run2_metEff, 4)))
     print("run2_orEff: ", str(round(run2_orEff, 4)))
     print("2018k_muEff: ", str(round(vbf2018k_muEff, 4)))
     print("2018k_vbfEff: ", str(round(vbf2018k_vbfEff, 4)))
     print("2018k_orEff: ", str(round(vbf2018k_orEff, 4)))
     print("\n")
  
  # draw histogram
  canvas = ROOT.TCanvas("c", "c", 10, 10, 800, 500)
  canvas.cd()
  h_trigEff.Draw()

  # write histogram to outfile
  outfile.cd()
  h_trigEff.Write() 
  outfile.Close()

# Removed MET cut for global efficiency
def run2_triggers(ttree, h_trigEff, vbffile, weight):
  '''if (ttree.muDPJTrig==1):
    h_trigEff.Fill("run2_muTrig", vbffile, weight)
    h_trigEff.Fill("run2_muTrig_unscaled", vbffile, 1)'''
  if (ttree.trig.at(22)==1):				   # tri-muon trigger
    h_trigEff.Fill("run2_3mu6Trig", vbffile, weight)
    h_trigEff.Fill("run2_3mu6Trig_unscaled", vbffile, 1)
  if (any([ttree.trig.at(t) for t in range(30, 36)])==1):  # any nScan trigger
    h_trigEff.Fill("run2_nScanTrig", vbffile, weight)
    h_trigEff.Fill("run2_nScanTrig_unscaled", vbffile, 1)
  if (ttree.metTrig==1): 
    h_trigEff.Fill("run2_metTrig", vbffile, weight)
    h_trigEff.Fill("run2_metTrig_unscaled", vbffile, 1)
  if (ttree.metTrig==1) or (any([ttree.trig.at(t) for t in range(30,36)])) or (ttree.trig.at(22)==1): 
    h_trigEff.Fill("run2_orTrig", vbffile, weight)
    h_trigEff.Fill("run2_orTrig_unscaled", vbffile, 1)
  return h_trigEff

# vbf only online in 2018
# require offline MET cut here to compare with 2SR
def muDPJvsVBF(ttree, h_trigEff, vbffile, weight, vbfPass):
  if (ttree.muDPJTrig==1):
    h_trigEff.Fill("2018k_muTrig", vbffile, weight)
    h_trigEff.Fill("2018k_muTrig_unscaled", vbffile, 1)
  if (ttree.vbfinclTrig==1 and vbfPass): 
    h_trigEff.Fill("2018k_vbfTrig", vbffile, weight)
    h_trigEff.Fill("2018k_vbfTrig_unscaled", vbffile, 1)
  if (ttree.muDPJTrig==1) or (ttree.vbfinclTrig==1 and vbfPass): 
    h_trigEff.Fill("2018k_orTrig", vbffile, weight)
    h_trigEff.Fill("2018k_orTrig_unscaled", vbffile, 1)
  return h_trigEff

def vbf_offline(ttree):  
  j1_pass = False
  j2_pass = False
  dijet_pass = False
  evt_isGood = False
  if (ttree.jet1_pt > 90e3 and abs(ttree.jet1_eta) < 3.2): j1_pass = True
  if (ttree.jet2_pt > 80e3 and abs(ttree.jet1_eta) < 4.9): j2_pass = True
  if (ttree.mjj > 1250e3 and abs(ttree.detajj) > 4.0 and abs(ttree.dphijj) < 2.0): dijet_pass = True 
  if (j1_pass == True and j2_pass == True and dijet_pass == True): evt_isGood = True 
  return evt_isGood
  
main()

