# Program written by Emily Pender (last edited 21/10/22) 
# Adapted from earlier muonic DPJ trigger efficiency code
# Compare trigger efficiency after hadDPJ SR selection
# 1. metTrig, hadDPJtrig and OR
# 2. metTrig, VBFtrig and OR
#--------------------------
# hadDPJ trigger: calRatio
# VBF trigger: inclusive
# MET trigger: all
#------------------------

import ROOT
import numpy as np
import csv

def main():
  
  # input/output info
  indir = '/Users/s1891044/Documents/Physics/DarkPhotons/source/v02-00/abcd-samples/'
  outdir = '/Users/s1891044/Documents/Physics/DarkPhotons/vbf-mudpj-trigger-studies/analysis-trigger-efficiencies/global-efficiencies/output/root/'
  outfile = ROOT.TFile.Open(outdir+"calo_out.root", "RECREATE")
  vbffiles = ['frvz_vbf_500757.root', 
	      'frvz_vbf_500758.root', 
	      'frvz_vbf_500759.root', 
              'frvz_vbf_500760.root', 
              'frvz_vbf_500761.root', 
              'frvz_vbf_500762.root']

  # save yields and efficiencies per sample in 2D histogram
  h_trigEff = ROOT.TH2D("h_trigEff", "h_trigEff", 16, 0, 16, len(vbffiles), 0, len(vbffiles)) 
  h_trigEff.Sumw2() 
  # loop over y axis
  print("\n")
  for inFile in vbffiles:
     print("RUNNING ON SAMPLE ", inFile, "...")
     print("----------------------------------------")
     print("\n")
     tfile = ROOT.TFile(indir+inFile)
     ttree = tfile.Get("miniT")
     vbffile = (inFile.split('_')[2]).split('.')[0]
     # initialise counters
     count = 0                                   # unscaled event count for testing 
     # loop through signal MC tree 
     for entry in range(ttree.GetEntries()):
       #if count > 100: continue
       count+=1
       ttree.GetEntry(entry)
       weight = ttree.scale1fb*ttree.intLumi
       # apply signal region selection 
       # total_SRcounts = SR(ttree, total_SRcounts)
       '''if (ttree.njet30<2) or (ttree.mjj<1e6) or (abs(ttree.detajj)<3): continue    # VBF filter
       if (ttree.nLJmus20>0) or (ttree.nLJjets20<1): continue		            # nmuDPJ selection
       if (ttree.MET<225e3): continue     				            # MET cut
       if (ttree.neleSignal!=0) or (ttree.nmuSignal!=0): continue                   # lepton veto 
       if (ttree.hasBjet!=0): continue					            # b-jet veto
       if (ttree.min_dphi_jetmet<0.4): continue					    # MET vs jet angular cut
       if (ttree.LJjet1_gapRatio<0.9) or (ttree.LJjet1_BIBtagger<0.2): continue	    # hDPJ quality cuts
       if (ttree.LJjet1_jvt>0.4) or (ttree.LJjet1_DPJtagger<0.95): continue	    # hDPJ quality cuts (continued) 
       if (abs(ttree.dphijj)>2.5): continue	     				    # dphijj cut''' 
       h_trigEff.Fill("run2_noTrig", vbffile, weight)		                   
       h_trigEff.Fill("run2_noTrig_unscaled", vbffile,  1)
       METvsCalRatio(ttree, h_trigEff, vbffile, weight)
       # events from period k in 2018 when vbf triggers are on
       if (ttree.RunNumber > 355529):
         h_trigEff.Fill("2018k_noTrig", vbffile, weight)
         h_trigEff.Fill("2018k_noTrig_unscaled", vbffile, 1)
         vbfPass = vbf_offline(ttree)
         METvsVBF(ttree, h_trigEff, vbffile, weight, vbfPass)

     # get yields from TH2 and print
     ybin = vbffiles.index(inFile)+1
     run2_noTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("run2_noTrig"), ybin)
     run2_metTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("run2_metTrig"), ybin)
     run2_calTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("run2_calTrig"), ybin)
     run2_orTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("run2_orTrig"), ybin)
     vbf2018k_noTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("2018k_noTrig"), ybin)
     vbf2018k_metTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("2018k_metTrig"), ybin)
     vbf2018k_vbfinclTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("2018k_vbfinclTrig"), ybin)
     vbf2018k_orTrig = h_trigEff.GetBinContent(h_trigEff.GetXaxis().FindBin("2018k_orTrig"), ybin)
     
     run2_noTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("run2_noTrig"), ybin)
     run2_metTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("run2_metTrig"), ybin)
     run2_calTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("run2_calTrig"), ybin)
     run2_orTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("run2_orTrig"), ybin)
     vbf2018k_noTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("2018k_noTrig"), ybin)
     vbf2018k_metTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("2018k_metTrig"), ybin)
     vbf2018k_vbfinclTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("2018k_vbfinclTrig"), ybin)
     vbf2018k_orTrig_err = h_trigEff.GetBinError(h_trigEff.GetXaxis().FindBin("2018k_orTrig"), ybin)

     print("run2_noTrig: ", str(round(run2_noTrig, 4)), "±", str(round(run2_noTrig_err, 4)))    
     print("run2_metTrig: ", str(round(run2_metTrig, 4)), "±", str(round(run2_metTrig_err, 4)))
     print("run2_calTrig: ", str(round(run2_calTrig, 4)), "±", str(round(run2_calTrig_err, 4)))
     print("run2_orTrig: ", str(round(run2_orTrig, 4)), "±", str(round(run2_orTrig_err, 4)))
     print("2018k_noTrig: ", str(round(vbf2018k_noTrig, 4)), "±", str(round(vbf2018k_noTrig_err, 4)))   
     print("2018k_metTrig: ", str(round(vbf2018k_metTrig, 4)), "±", str(round(vbf2018k_metTrig_err, 4)))  
     print("2018k_vbfinclTrig: ", str(round(vbf2018k_vbfinclTrig, 4)), "±", str(round(vbf2018k_vbfinclTrig_err, 4)))
     print("2018k_orTrig: ", str(round(vbf2018k_orTrig, 4)), "±", str(round(vbf2018k_orTrig_err, 4)))
     print("\n")
     
     # calculate trigger efficiencies, fill histogram and print
     run2_metEff = (run2_metTrig/run2_noTrig)      
     run2_calEff = (run2_calTrig/run2_noTrig)      
     run2_orEff = (run2_orTrig/run2_noTrig)      
     vbf2018k_metEff = (vbf2018k_metTrig/vbf2018k_noTrig)      
     vbf2018k_vbfEff = (vbf2018k_vbfinclTrig/vbf2018k_noTrig)      
     vbf2018k_orEff = (vbf2018k_orTrig/vbf2018k_noTrig)      
      
     h_trigEff.Fill("run2_METTrig_eff", vbffile, run2_metEff)      
     h_trigEff.Fill("run2_calRatioTrig_eff", vbffile, run2_calEff)      
     h_trigEff.Fill("run2_ORTrig_eff", vbffile, run2_orEff)      
     h_trigEff.Fill("2018k_metTrig_eff", vbffile, vbf2018k_metEff)      
     h_trigEff.Fill("2018k_vbfinclTrig_eff", vbffile,vbf2018k_vbfEff)      
     h_trigEff.Fill("2018k_orTrig_eff", vbffile, vbf2018k_orEff)      
   
     print("run2_metEff: ", str(round(run2_metEff, 4)))
     print("run2_calEff: ", str(round(run2_calEff, 4)))
     print("run2_orEff: ", str(round(run2_orEff, 4)))
     print("2018k_metEff: ", str(round(vbf2018k_metEff, 4)))
     print("2018k_vbfEff: ", str(round(vbf2018k_vbfEff, 4)))
     print("2018k_orEff: ", str(round(vbf2018k_orEff, 4)))
     print("\n")
  
  # draw histogram
  canvas = ROOT.TCanvas("c", "c", 10, 10, 800, 500)
  canvas.cd()
  h_trigEff.Draw()

  # write histogram to outfile
  outfile.cd()
  h_trigEff.Write() 
  outfile.Close()


# vbf only online in 2018
def METvsVBF(ttree, h_trigEff, vbffile, weight, vbfPass):
  if (ttree.vbfinclTrig==1 and vbfPass):
    h_trigEff.Fill("2018k_vbfinclTrig", vbffile, weight)
    h_trigEff.Fill("2018k_vbfinclTrig_unscaled", vbffile, 1)
  if (ttree.metTrig==1): 
    h_trigEff.Fill("2018k_metTrig", vbffile, weight)
    h_trigEff.Fill("2018k_metTrig_unscaled", vbffile, 1)
  if (ttree.metTrig==1) or (ttree.vbfinclTrig == 1 and vbfPass): 
    h_trigEff.Fill("2018k_orTrig", vbffile, weight)
    h_trigEff.Fill("2018k_orTrig_unscaled", vbffile, 1)
  return h_trigEff

def METvsCalRatio(ttree, h_trigEff, vbffile, weight):
  if (ttree.metTrig==1):
    h_trigEff.Fill("run2_metTrig", vbffile, weight)
    h_trigEff.Fill("run2_metTrig_unscaled", vbffile, 1)
  if (ttree.hadDPJTrig==1): 
    h_trigEff.Fill("run2_calTrig", vbffile, weight)
    h_trigEff.Fill("run2_calTrig_unscaled", vbffile, 1)
  if (ttree.hadDPJTrig==1) or (ttree.metTrig == 1): 
    h_trigEff.Fill("run2_orTrig", vbffile, weight)
    h_trigEff.Fill("run2_orTrig_unscaled", vbffile, 1)
  return h_trigEff

def vbf_offline(ttree):  
  j1_pass = False
  j2_pass = False
  dijet_pass = False
  evt_isGood = False
  if (ttree.jet1_pt > 90e3 and abs(ttree.jet1_eta) < 3.2): j1_pass = True
  if (ttree.jet2_pt > 80e3 and abs(ttree.jet1_eta) < 4.9): j2_pass = True
  if (ttree.mjj > 1250e3 and abs(ttree.detajj) > 4.0 and abs(ttree.dphijj) < 2.0): dijet_pass = True 
  if (j1_pass == True and j2_pass == True and dijet_pass == True): evt_isGood = True 
  return evt_isGood
  
main()

