# Script to transform output csv file to latex table format
# To run: $python maketable.py muonic

import ROOT
import csv
import sys 

def main():

  channel = sys.argv[1]
  infile = '/Users/s1891044/Documents/Physics/DarkPhotons/vbf-mudpj-trigger-studies/analysis-trigger-efficiencies/global-efficiencies/output/root/' + channel + '_out.root'
  with open('output/latex/' + channel + '_table.txt', 'w') as outfile:
    outfile.write('\\begin{table}\n')
    outfile.write('  \\resizebox{\\linewidth}{!}{\n')
    outfile.write('  \\centering\n')
    outfile.write('  \\begin{tabular}{|c|c|c|c|')
    # create header
    header = '    $\mathrm{m_{\gamma_d}}$ [GeV] & c$\mathrm{\\tau_{\gamma_d}}$ [mm] & $\mathrm{m_{HLSP}}$ [GeV] & $\mathrm{m_{fd2}} [GeV]$'
    trigCols = []
    tfile = ROOT.TFile(infile)
    h_trigEff = tfile.Get("h_trigEff")
    nFiles = h_trigEff.GetYaxis().GetNbins()
    # loop through files (hist y axis --> table rows)
    for file_idx in range(nFiles):
      fileName = h_trigEff.GetYaxis().GetBinLabel(file_idx+1)
      # get n extra columns and names of triggers
      # get table header and write
      if file_idx == 0: 
        for bin_idx in range(h_trigEff.GetXaxis().GetNbins()):   # note: there are 44 bins, loads of empty at end
          binName = h_trigEff.GetXaxis().GetBinLabel(bin_idx)
          if "eff" in binName and "2018" not in binName: trigCols.append(bin_idx)
        for n_col in trigCols:
          trigName = h_trigEff.GetXaxis().GetBinLabel(n_col)
          header += " & " + trigName.split("_")[1].split("Trig")[0] + " Efficiency" 
        for n in range(len(trigCols)): outfile.write("c|") 
        outfile.write('}\n    \\hline \n')
        outfile.write(header + ' \\\\ \n    \\hline \n')
      # for each file, write row of frvz parameters and trigger efficiencies
      outfile.write("    " + getLine(fileName))
      for col_idx in trigCols: outfile.write(" & " + str(round(h_trigEff.GetBinContent(col_idx, file_idx+1)*100, 2)) + "\\%")
      outfile.write(' \\\\ \n')
      if file_idx == nFiles-1: outfile.write('    \\hline \n')
    outfile.write('  \\end{tabular}} \n')
    outfile.write('  \\caption{Global signal efficiencies for ' + channel + ' channel triggers} \n')
    outfile.write('\\end{table}')
    print("\n")
    print("Latex table created at output/latex/"+channel+"_table.txt")
    print("\n")
  
def getLine(fileName):
  if '500757' in fileName: line = "0.1 & 15 & 2 & 5"  
  if '500758' in fileName: line = "0.4 & 50 & 2 & 5"  
  if '500759' in fileName: line = "0.4 & 5 & 2 & 5" 
  if '500760' in fileName: line = "0.4 & 500 & 2 & 5"  
  if '500761' in fileName: line = "10 & 900 & 10 & 35"  
  if '500762' in fileName: line = "15 & 1000 & 10 & 45"  
  return line
   
main() 
