# Just a wee script to write output of trigEff to a csv file
# Written on 05/05/2022

import ROOT
import csv
import sys

channel = sys.argv[1]
infile = '/Users/s1891044/Documents/DarkPhotons/vbf-mudpj-trigger-studies/output/' + channel + '_out.root'

with open('output/' + channel + '_table.csv', 'w') as outfile:
  writer = csv.writer(outfile)
  tfile = ROOT.TFile(infile)
  h_trigEff = tfile.Get("h_trigEff")
  nRows = h_trigEff.GetXaxis().GetNbins()
  nFiles = h_trigEff.GetYaxis().GetNbins()
  header=["sample"]
    
  # loop over vbf samples
  for fileIdx in range(nFiles):
    fileName = h_trigEff.GetYaxis().GetBinLabel(fileIdx+1)
    row = [fileName]
    # loop over yields/efficiencies
    for varIdx in range(1, nRows+1):
      label = h_trigEff.GetXaxis().GetBinLabel(varIdx)
      if "unscaled" in label: continue
      if fileIdx == 0: header.append(label)
      # get array value
      var = h_trigEff.GetBinContent(varIdx, fileIdx+1)
      err = h_trigEff.GetBinError(varIdx, fileIdx+1)
      if "eff" in label: row.append(str(round(var, 3)))
      else: row.append(str(round(var, 1)) + "±" + str(round(err, 1)))
    if fileIdx == 0: writer.writerow(header)
    writer.writerow(row)

print("\n")
print("csv file successfully written to output/" + channel + "_table.csv")	 
print("\n")
